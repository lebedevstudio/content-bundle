<?php

declare(strict_types=1);

namespace lst\ContentBundle\Controller;

use lst\ContentBundle\Entity\Article;
use lst\ContentBundle\Entity\Category;
use lst\ContentBundle\Repository\ArticleRepository;
use lst\ContentBundle\Repository\CategoryRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\Link;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Service\Breadcrumbs\Breadcrumbs;
use lst\CoreBundle\Service\Breadcrumbs\Crumb;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ArticlesController extends AbstractController
{
    /** @var CategoryRepository */
    private $categoryRepository;
    /** @var ArticleRepository */
    private $articleRepository;

    /** @var Operations */
    protected $operations;

    public function __construct(
        Operations $operations, NormalizerInterface $normalizer, RequestStack $request,
        CategoryRepository $categoryRepository, ArticleRepository $articleRepository)
    {
        $this->operations = $operations;
        $this->categoryRepository = $categoryRepository;
        $this->articleRepository = $articleRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/content/articles", name="content.article.list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getArticles() : JsonResponse
    {
        return $this->list(Article::class, Article::MULTIPLE_KEY);
    }

    /**
     * @Route("/content/articles/{id}", name="content.article.get", methods={"GET"}, requirements={"id"="\d+"})
     * @param Article $article
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getArticle(Article $article) : JsonResponse
    {
        return new JsonResponse([
            Article::SINGLE_KEY => $this->normalizer->normalize($article, 'array', [
                'groups' => $this->serializationGroups
            ]),
            Breadcrumbs::KEY => $this->normalizer->normalize(
                $this->collectBreadcrumbs($article), 'array'
            ),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($article), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route("/content/articles/alias/{alias}", name="content.article.get.by.alias", methods={"GET"}, requirements={"\d+"})
     * @param Article $article
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getArticleByAlias(Article $article) : JsonResponse
    {
        $data = [
            Article::SINGLE_KEY => $this->normalizer->normalize($article, 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
            'navigation' => [
                'next' => $this->normalizer->normalize(
                    $this->articleRepository->getNext($article), 'array', [
                    'groups' => ['basic']
                ]),
                'previous' => $this->normalizer->normalize(
                    $this->articleRepository->getPrevious($article), 'array', [
                    'groups' => ['basic']
                ]),
            ],
            Breadcrumbs::KEY => $this->normalizer->normalize(
                $this->collectBreadcrumbs($article), 'array'
            ),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($article), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ];

        return new JsonResponse($data, $this->responseStatus);
    }
    
    /**
     * @Route("/content/articles/{id}", name="content.article.update", methods={"PUT"}, requirements={"id"="\d+"})
     * @param Article $article
     *
     * @return JsonResponse
     */
    public function updateArticle(Article $article) : JsonResponse
    {
        return $this->persist(Article::class, Article::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/content/articles", name="content.article.create", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function createArticle() : JsonResponse
    {
        return $this->persist(Article::class, Article::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/content/articles/{id}", name="content.article.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     *
     * @param Article $article
     * @return JsonResponse
     */
    public function deleteArticle(Article $article) : JsonResponse
    {
        return $this->delete($article);
    }

    private function collectBreadcrumbs(EntityTypeInterface $entity) : array
    {
        $breadcrumbs = new Breadcrumbs();
        $breadcrumbs->addCrumb(new Crumb(
                $entity->getId(), $entity->getTitle(), $entity->getAlias(), $entity::getEntityTypeId()
            )
        );
        $parents = $this->categoryRepository->getParents($entity->getCategory());
        /** @var Category $category */
        foreach ($parents as $category) {
            $breadcrumbs->addCrumb(new Crumb(
                    $category->getId(), $category->getTitle(), $category->getAlias(), Category::getEntityTypeId()
                )
            );
        }

        return $breadcrumbs->getCrumbs();
    }
}
