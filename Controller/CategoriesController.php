<?php

declare(strict_types=1);

namespace lst\ContentBundle\Controller;

use lst\ContentBundle\Entity\Category;
use lst\ContentBundle\Repository\CategoryRepository;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\Link;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Service\Breadcrumbs\Breadcrumbs;
use lst\CoreBundle\Service\Breadcrumbs\Crumb;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CategoriesController extends AbstractController
{
    /** @var CategoryRepository */
    private $categoryRepository;

    /** @var Operations */
    protected $operations;

    public function __construct(
        Operations $operations, NormalizerInterface $normalizer, RequestStack $request, CategoryRepository $categoryRepository)
    {
        $this->operations = $operations;
        $this->categoryRepository = $categoryRepository;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/content/categories", name="content.category.list", methods={"GET"})
     */
    public function getCategoriesList() : JsonResponse
    {
        return $this->list(Category::class, Category::MULTIPLE_KEY);
    }

    /**
     * @Route("/content/categories/{id}", name="content.category.get", methods={"GET"}, requirements={"id"="\d+"})
     * @param Category $category
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getCategory(Category $category) : JsonResponse
    {
        $category->showInActive();

        return new JsonResponse([
            Category::SINGLE_KEY => $this->normalizer->normalize($category, 'array', [
                'groups' => $this->serializationGroups
            ]),
            Breadcrumbs::KEY => $this->normalizer->normalize(
                $this->collectBreadcrumbs($category), 'array'
            ),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($category), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route("/content/categories/alias/{alias}", name="content.category.get.by.alias", methods={"GET"}, requirements={"\d+"})
     * @param Category $category
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getCategoryByAlias(Category $category) : JsonResponse
    {
        return new JsonResponse([
            Category::SINGLE_KEY => $this->normalizer->normalize($category,'array', [
                'groups' => $this->serializationGroups,
//                'enable_max_depth' => true,
            ]),
            Breadcrumbs::KEY => $this->normalizer->normalize(
                $this->collectBreadcrumbs($category), 'array'
            ),
            Link::MULTIPLE_KEY => $this->normalizer->normalize(
                $this->getLinks($category), 'array', [
                    'groups' => $this->serializationGroups
                ]
            ),
        ], $this->responseStatus);
    }

    /**
     * @Route("/content/categories/{id}", name="content.category.update", methods={"PUT"}, requirements={"id"="\d+"})
     * @param Category $c
     *
     * @return JsonResponse
     */
    public function updateCategory(Category $c) : JsonResponse
    {
        return $this->persist(Category::class, Category::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/content/categories", name="content.category.create", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function createCategory() : JsonResponse
    {
        return $this->persist(Category::class, Category::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/content/categories/{id}", name="content.category.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @param Category $category
     *
     * @return JsonResponse
     */
    public function deleteCategory(Category $category) : JsonResponse
    {
        return $this->delete($category);
    }

    private function collectBreadcrumbs(EntityTypeInterface $entity) : array
    {
        $breadcrumbs = new Breadcrumbs();
        $breadcrumbs->addCrumb(new Crumb(
                $entity->getId(), $entity->getTitle(), $entity->getAlias(), $entity::getEntityTypeId()
            )
        );
        $parents = $this->categoryRepository->getParents($entity->getParent());
        /** @var EntityTypeInterface $category */
        foreach ($parents as $category) {
            $breadcrumbs->addCrumb(new Crumb(
                    $category->getId(), $category->getTitle(), $category->getAlias(), Category::getEntityTypeId()
                )
            );
        }

        return $breadcrumbs->getCrumbs();
    }
}
