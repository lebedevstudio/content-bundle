<?php

declare(strict_types=1);

namespace lst\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Validator\Constraints as Asserts;
use lst\CoreBundle\Entity\PageMeta;
use lst\MediaBundle\Entity\File;
use lst\MediaBundle\Entity\Gallery;

/**
 * Content Article Entity
 *
 * @ORM\Table(
 *     name="content_articles",
 *     indexes={
 *          @ORM\Index(name="IDX_CONTENT_ARTICLES_LOCALE_INDEX", columns={"locale"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="lst\ContentBundle\Repository\ArticleRepository")
 */
class Article extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 1;
    /** @var string */
    public const SINGLE_KEY = 'article';
    /** @var string */
    public const MULTIPLE_KEY = 'articles';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @Groups({"list"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Asserts\UniqueField()
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Groups({"basic"})
     *
     */
    protected $alias;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=1024
     * )
     * @ORM\Column(type="string", length=1024)
     * @Groups({"basic"})
     */
    protected $title;

    /**
     * @Assert\Length(
     *     max=1024
     * )
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups({"basic"})
     */
    protected $subtitle = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"basic"})
     */
    protected $description = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"basic"})
     */
    protected $content = '';

    /**
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @MaxDepth(1)
     * @Groups({"category"})
     */
    private $category = null;

    /**
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * @Groups({"image"})
     */
    private $image = null;

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\Gallery")
     * @Groups({"gallery"})
     */
    protected $gallery = null;

    /**
     * @ORM\OneToOne(targetEntity="lst\CoreBundle\Entity\PageMeta")
     * @Groups({"basic"})
     */
    protected $pageMeta = null;

    /**
     * @ORM\Column(type="boolean", options={"default":"true"})
     * @Groups({"basic"})
     */
    protected $active = true;

//    public function __construct()
//    {
//        $this->createdAt = new \DateTimeImmutable();
//    }

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias) : void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getContent() : ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     */
    public function setContent(?string $content) : void
    {
        $this->content = $content;
    }

    /**
     * @return File|null
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery() : ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @param Gallery|null $gallery
     */
    public function setGallery(?Gallery $gallery) : void
    {
        $this->gallery = $gallery;
    }

    /**
     * @return PageMeta|null
     */
    public function getPageMeta()
    {
        return $this->pageMeta;
    }

    /**
     * @param PageMeta|null $pageMeta
     */
    public function setPageMeta(?PageMeta $pageMeta) : void
    {
        $this->pageMeta = $pageMeta;
    }

    /**
     * @param Category $category
     */
    public function setCategory(?Category $category) : void
    {
        $this->category = $category;
    }

    /**
     * @return Category
     */
    public function getCategory() : ?Category
    {
        return $this->category;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active) : void
    {
        $this->active = $active; 
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }
}
