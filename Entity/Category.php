<?php

declare(strict_types=1);

namespace lst\ContentBundle\Entity;

use lst\MediaBundle\Entity\File;
use lst\MediaBundle\Entity\Gallery;
use lst\CoreBundle\Validator\Constraints as Asserts;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Entity\PageMeta;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * Content Category Entity
 *
 * @ORM\Table(
 *     name="content_categories",
 *     indexes={
 *          @ORM\Index(name="IDX_CONTENT_CATEGORIES_LOCALE_INDEX", columns={"locale"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="lst\ContentBundle\Repository\CategoryRepository")
 */
class Category extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 2;
    /** @var string */
    public const SINGLE_KEY = 'category';
    /** @var string */
    public const MULTIPLE_KEY = 'categories';
    /** @var bool */
    private $showInActive = false;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=255
     * )
     * @Asserts\UniqueField()
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Groups({"basic"})
     */
    protected $alias;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=1024
     * )
     * @ORM\Column(type="string", length=1024, nullable=false)
     * @Groups({"basic"})
     */
    protected $title;

    /**
     * @Assert\Length(
     *     max=1024
     * )
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups({"basic"})
     */
    protected $subtitle = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"basic"})
     */
    protected $description = '';

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\File")
     * @Groups({"image"})
     */
    protected $image = null;

    /**
     * @ORM\ManyToOne(targetEntity="lst\MediaBundle\Entity\Gallery")
     * @Groups({"gallery"})
     */
    protected $gallery = null;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children", fetch="EAGER")
     * @MaxDepth(1)
     * @Groups({"parent"})
     */
    protected $parent = null;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", fetch="EXTRA_LAZY")
     * @MaxDepth(1)
     * @Groups({"children"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category", fetch="EXTRA_LAZY")
     * @MaxDepth(1)
     * @Groups({"articles"})
     */
    private $articles;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":"true"})
     * @Groups({"basic"})
     */
    private $active = true;

    /**
     * @ORM\OneToOne(targetEntity="lst\CoreBundle\Entity\PageMeta")
     * @Groups({"basic"})
     */
    protected $pageMeta = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias) : void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|string $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery() : ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @param Gallery|null $gallery
     */
    public function setGallery(?Gallery $gallery) : void
    {
        $this->gallery = $gallery;
    }

    public function getParent() : ?Category
    {
        return $this->parent;
    }

    /**
     * @param Category|null $parent
     */
    public function setParent(?Category $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        $criteria = Criteria::create();
        if ($this->showInActive != true) {
            $criteria->where(Criteria::expr()->eq('active', true));
        }

        return $this->children->matching($criteria);
    }

    /**
     * @return ArrayCollection
     */
    public function getArticles()
    {
        $criteria = Criteria::create();
        if ($this->showInActive != true) {
            $criteria->where(Criteria::expr()->eq('active', true));
        }

        return $this->articles->matching($criteria);
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active) : void
    {
        $this->active = $active;
    }

    /**
     * @return PageMeta|null
     */
    public function getPageMeta()
    {
        return $this->pageMeta;
    }

    /**
     * @param PageMeta|null $pageMeta
     */
    public function setPageMeta(?PageMeta $pageMeta) : void
    {
        $this->pageMeta = $pageMeta;
    }

    /**
     * @param bool $show
     */
    public function showInActive(bool $show = true) : void
    {
        $this->showInActive = $show;
    }
}
