<?php

namespace lst\ContentBundle\Repository;

use lst\ContentBundle\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager 
     */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();
        
        parent::__construct($registry, Article::class);
    }
    
    public function persist(Article $article) : Article
    {
        $entry = $this->em->merge($article);
        $this->em->flush();
        
        return $entry;
    }

    public function delete(Article $article) : void
    {
        $this->em->remove($article);
        $this->em->flush();
    }

    public function getNext(Article $article) : ?Article
    {
        return $this->em->createQuery('
            SELECT u FROM ' . Article::class . ' u 
            WHERE u.id = (
                SELECT MIN(us.id) FROM ' . Article::class . ' us
                    WHERE us.id > :id AND us.active = true AND us.category = :category
            )
        ')
            ->setParameter(':id', $article->getId())
            ->setParameter(':category', $article->getCategory() ? $article->getCategory()->getId() : null)
            ->getOneOrNullResult();
    }

    public function getPrevious(Article $article) : ?Article
    {
        return $this->em->createQuery('
            SELECT u FROM ' . Article::class . ' u
            WHERE u.id = (
                SELECT MAX(us.id) FROM ' . Article::class . ' us
                    WHERE us.id < :id AND us.active = true AND us.category = :category
            )
        ')
            ->setParameter(':id', $article->getId())
            ->setParameter(':category', $article->getCategory() ? $article->getCategory()->getId() : null)
            ->getOneOrNullResult();
    }
}
